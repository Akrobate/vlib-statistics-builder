FROM debian:bullseye

RUN apt update
RUN apt-get install git python3 python3-pip python3-pandas -y

# RUN pip install git+https://framagit.org/Akrobate/python-vlib-api.git
# RUN pip install coverage

RUN mkdir /vlib-statistics-builder
COPY ./requirements.txt /vlib-statistics-builder/
RUN pip install -r /vlib-statistics-builder/requirements.txt

WORKDIR /vlib-statistics-builder


