from src.csv_file_manager import saveStatisticsDataframe
import pandas as pd
from datetime import datetime
from os import path


def test_saveStatisticsDataframe_when_file_exists(monkeypatch):

    def path_exists(*args, **kwargs):
        return True

    def df_to_csv(*args, **kwargs):
        (file_path_arg, ) = args
        file_path_expected = '/output/path/' + datetime.now().strftime("%Y-%m-%d") + '.csv'
        assert file_path_arg == file_path_expected
        assert kwargs['header'] == False
        assert kwargs['mode'] == 'a'
        return True

    input_data = pd.DataFrame([{'a':1}])

    monkeypatch.setattr(path, "exists", path_exists)
    monkeypatch.setattr(input_data, "to_csv", df_to_csv)

    saveStatisticsDataframe(input_data, '/output/path')


def test_saveStatisticsDataframe_when_file_does_not_exists(monkeypatch):

    def path_exists(*args, **kwargs):
        return False

    def df_to_csv(*args, **kwargs):
        (file_path_arg, ) = args
        file_path_expected = '/output/path/' + datetime.now().strftime("%Y-%m-%d") + '.csv'
        assert file_path_arg == file_path_expected
        assert kwargs['header'] == True
        return True

    input_data = pd.DataFrame([{'a':1}])

    monkeypatch.setattr(path, "exists", path_exists)
    monkeypatch.setattr(input_data, "to_csv", df_to_csv)

    saveStatisticsDataframe(input_data, '/output/path')

