from src.formatter import flattifyDictionnary, generateFileNameFromDateTime, prepareData
from dateutil import parser
import pandas as pd
import numpy as np

seed_dictionnary_to_format = {
    'id': 1,
    'prop1': 'Property 1 value',
    'prop2': 'Property 2 value',
    'prop3': 'Property 3 value',
    'prop4': {
        'subprop1': 'Prop 4 subprop 1 value',
        'subprop2': 'Prop 4 subprop 2 value'
    }
}

seed_collection_dictionnaries_to_format = [
    {
        'id': 1,
        'prop1': 'Property 1 value',
        'prop2': 'Property 2 value',
        'prop3': 'Property 3 value',
        'prop4': {
            'subprop1': 'Prop 4 subprop 1 value',
            'subprop2': 'Prop 4 subprop 2 value'
        }
    },
    {
        'id': 2,
        'prop1': 'Property 1 value',
        'prop2': 'Property 2 value',
        'prop3': 'Property 3 value',
        'prop4': {
            'subprop1': 'Prop 4 subprop 1 value',
            'subprop2': 'Prop 4 subprop 2 value'
        }
    }
]

def test_flattify_dictionnary():

    result = seed_dictionnary_to_format
    response = flattifyDictionnary(result)
    
    # check properties
    assert 'id' in response
    assert 'prop1' in response
    assert 'prop4_subprop1' in response
    assert 'prop4_subprop2' in response

    # check values
    assert response['id'] == seed_dictionnary_to_format['id']
    assert response['prop4_subprop1'] == seed_dictionnary_to_format['prop4']['subprop1']


def test_flattify_collection_of_dictionnaries():

    result = seed_dictionnary_to_format
    response = flattifyDictionnary(result)
    
    # check properties
    assert 'id' in response
    assert 'prop1' in response
    assert 'prop4_subprop1' in response
    assert 'prop4_subprop2' in response

    # check values
    assert response['id'] == seed_dictionnary_to_format['id']
    assert response['prop4_subprop1'] == seed_dictionnary_to_format['prop4']['subprop1']


def test_generateFileNameFromDateTime():

    datetime_string = "21 May, 1985"
    date_time = parser.parse(datetime_string)
    filename = generateFileNameFromDateTime(date_time)
    # check values
    assert filename == '1985-05-21'

def test_generateFileNameFromDateTime_with_extention():
    
    datetime_string = "21 May, 1985"
    date_time = parser.parse(datetime_string)
    filename = generateFileNameFromDateTime(date_time, 'csv')
    # check values
    assert filename == '1985-05-21.csv'

def test_prepareData():
    result = prepareData(seed_collection_dictionnaries_to_format)
    assert type(result) == pd.DataFrame
    assert type(result.datetime) == pd.core.series.Series

