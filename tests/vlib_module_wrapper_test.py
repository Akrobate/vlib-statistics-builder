# pylint: disable=import-error,
from src.vlib_module_wrapper import getVlibData
import vlib_api as vlibapi

def test_getVlibData(monkeypatch):

    def vlib_api_getAllStations(*args, **kwargs):
        return True

    monkeypatch.setattr(vlibapi, "getAllStations", vlib_api_getAllStations)
    assert getVlibData() == True

