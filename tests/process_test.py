import pandas as pd
from src import logger
from src import process

def test_process(monkeypatch):

    getVlibData_called_count = 0
    saveStatisticsDataframe_called_count = 0
    loadCurrentStatisticDataframe_called_count = 0
    prepareData_called_count = 0
    loggerInfo_called_count = 0

    def getVlibData_mock(*args, **kwargs):
        nonlocal getVlibData_called_count
        getVlibData_called_count += 1

    def prepareData_mock(*args, **kwargs):
        nonlocal prepareData_called_count
        prepareData_called_count += 1
        return pd.DataFrame()

    def saveStatisticsDataframe_mock(*args, **kwargs):
        nonlocal saveStatisticsDataframe_called_count
        saveStatisticsDataframe_called_count += 1

    def loggerInfo_mock(*args, **kwargs):
        nonlocal loggerInfo_called_count
        loggerInfo_called_count += 1

    def loadCurrentStatisticDataframe_mock(*args, **kwargs):
        nonlocal loadCurrentStatisticDataframe_called_count
        loadCurrentStatisticDataframe_called_count += 1
        return pd.DataFrame()


    monkeypatch.setattr(process, "getVlibData", getVlibData_mock)
    monkeypatch.setattr(process, "prepareData", prepareData_mock)
    monkeypatch.setattr(process, "saveStatisticsDataframe", saveStatisticsDataframe_mock)
    monkeypatch.setattr(process, "loadCurrentStatisticDataframe", loadCurrentStatisticDataframe_mock)
    monkeypatch.setattr(logger, "info", loggerInfo_mock)

    process.process('/data/path')

    assert getVlibData_called_count == 1
    assert saveStatisticsDataframe_called_count == 1
    assert loadCurrentStatisticDataframe_called_count == 1
    assert prepareData_called_count == 1
    assert loggerInfo_called_count == 3

