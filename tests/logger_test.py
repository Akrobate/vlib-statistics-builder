from datetime import datetime
from src import logger

def test_info_logger(monkeypatch):

    message = "My log message"
    expected = datetime.now().strftime("%Y-%m-%d %H:%M:%S") + ' - ' + message
    
    def printConsole(*args, **kwargs):
        nonlocal expected
        (message, ) = args
        assert message == expected

    monkeypatch.setattr(logger, "printConsole", printConsole)

    logger.info(message)
