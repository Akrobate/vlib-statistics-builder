from src.formatter import generateFileNameFromDateTime
from datetime import datetime
from os import path
import pandas as pd

def saveStatisticsDataframe(dataframe, output_path):
    filename_full_path = generateCurrentFileName(output_path)
    if path.exists(filename_full_path):
        dataframe.to_csv(filename_full_path, index=False, mode='a', header=False)
    else:
        dataframe.to_csv(filename_full_path, index=False, header=True)

def loadCurrentStatisticDataframe(output_path):
    filename_full_path = generateCurrentFileName(output_path)
    return loadStatisticsDataframe(filename_full_path)

def loadStatisticsDataframe(file_name):
    return pd.read_csv(file_name)

def generateCurrentFileName(output_path):
    output_file_name = generateFileNameFromDateTime(datetime.now(), 'csv')
    return path.join(output_path, output_file_name)
    