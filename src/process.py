from src.formatter import prepareData
from src.vlib_module_wrapper import getVlibData
from src.csv_file_manager import saveStatisticsDataframe, loadCurrentStatisticDataframe
from src import logger

def process(output_path):
    logger.info('Downloading file...')
    all_stations_data = getVlibData()
    dataframe = prepareData(all_stations_data)
    logger.info('Data download : ' + str(len(dataframe.index)) + ' lines - done')
    saveStatisticsDataframe(dataframe, output_path)
    logger.info('Data saved ' + str(len(loadCurrentStatisticDataframe(output_path).index)) + ' lines - done')

