import pandas as pd
from datetime import datetime


def prepareData(all_stations_data):
    df = pd.DataFrame(flattifyDictionnariesList(all_stations_data))
    df['datetime'] = datetime.now()
    return df


def flattifyDictionnary(object, separator = '_'):

    result_dict = {}
    def processBranch(element, path = ''):
        if type(element) is dict:
            for key in element:
                new_path = path + separator + key
                if path == '':
                    new_path = key
                processBranch(element[key], new_path)
        else:
            result_dict[path] = element
        return element

    processBranch(object)

    return result_dict


def flattifyDictionnariesList(collection, separator = '_'):
    result = []
    for element in collection:
        result.append(flattifyDictionnary(element, separator))
    return result


def generateFileNameFromDateTime(date_time_object, extention = ''):
    result = date_time_object.strftime("%Y-%m-%d")
    if extention != '':
        result = result + '.' + extention
    return result
