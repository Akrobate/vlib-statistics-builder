#!/bin/bash

DOCKER_IMAGE_NAME="vlib-statistics-builder"

docker build -t $DOCKER_IMAGE_NAME .
docker run -v `pwd`/:/vlib-statistics-builder -i -t $DOCKER_IMAGE_NAME bash

