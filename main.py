from os import path
from src.process import process

def main():
    output_path = path.join(path.dirname(__file__), 'data')
    process(output_path)

if __name__ == "__main__":
    main()
