# vlib-statistics-builder

## Using

### Build the project within Docker

```bash
# Building docker and checking
./bash-start.sh

# Executing data collect
./run-in-docker.sh
```

### Crontab to periodically get data

```
*/1 * * * *     sh run-in-docker.sh
```

## Dev

### Testing the project

```bash
# Just testing
py.test

# testing and let it show prints for debug
py.test -s
```
### Coverage of the project

```bash
# testing and coverage run
coverage run --source="./src/" -m py.test

# viewing coverage report
coverage report
```
